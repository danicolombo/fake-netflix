# Codo a codo

## TPs for Codo a codo's course

### Fake Netflix

Developed with react, materia ui & react-router-dom.

### [Fetching a resource][3]

Developed with react & react-router-dom.

## Build

Inside netflix directory, install the dependencias and you can run:

```
yarn start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## omdbapi

Finally set your `API_KEY` ([omdbapi][2] mock api) a in .env inside `./netflix/`

## Libraries

It was developed with the following libraries:

* "react-router-dom": "6.3.0"
* "axios": "0.27.2"
* "@material-ui/core": "4.12.4"
* "@material-ui/icons": "4.11.3"

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

* "react": "18.1.0"

Fonts used: [Roboto][0].

## [MIT][1] License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
NCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[0]: https://fonts.google.com/specimen/Roboto
[1]: https://opensource.org/licenses/MIT
[2]: http://www.omdbapi.com
[3]: https://gitlab.com/danicolombo/fetching-a-resource
