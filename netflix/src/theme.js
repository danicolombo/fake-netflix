import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

const netflixBlack = "#0C0B10";
const netflixPink = "#FC676B";

let theme = createTheme({
    palette: {
        primary: {
            main: `${netflixBlack}`,
        },
        secondary: {
            main: `${netflixPink}`,
        },
    },
    typography: {
        h1: {
            fontWeight: 900,
            fontFamly: 'Raleway',
            fontSize: '4rem',
            paddingTop: '5px',
        },
        h2: {
            fontWeight: 300,
            fontFamly: 'Raleway',
            fontSize: '4rem',
            paddingTop: '5px',
            color: 'red',
        },
        h3: {
            fontWeight: 300,
            fontFamly: 'Raleway',
            fontSize: '2rem',
            paddingTop: '5px',
        },
        h4: {
            fontWeight: 500,
            fontFamly: 'Raleway',
            fontSize: '24px',
            paddingTop: '5px',
        },
        h5: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '14px',
            paddingTop: '5px',
        },
        h6: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '12px',
            paddingTop: '5px',
        },
        body1: {
            fontSize: '16px',
            fontWeight: 600,
            fontFamly: 'Raleway',
            paddingTop: '5px',
            color: '#FFF',
        },
        body2: {
            fontSize: '14px',
            fontWeight: 300,
            fontFamly: 'Raleway',
            paddingTop: '5px',
            color: '#FFF',
        },
    },
});

theme = responsiveFontSizes(theme);

export default theme;
