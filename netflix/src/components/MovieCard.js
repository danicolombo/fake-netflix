import React, {useState, useEffect} from 'react';
import {makeStyles, createStyles} from '@material-ui/core/styles';
import {Card, Grid, CardHeader, CardMedia, IconButton, Typography} from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import noPoster from '../assets/no-film.jpg';
import {useNavigate} from 'react-router-dom';
import {useStickyState} from '../hooks/UseStickyState';

const useStyles = makeStyles((theme) =>
    createStyles({
        root: {
            backgroundColor: 'inherit',
            padding: theme.spacing(3),
            margin: theme.spacing(1),
            cursor: 'pointer',
            display: 'flex',
            flexDirection: 'column',
        },
        media: {
            height: 0,
            padding: '56% 0',
        },
        header: {
            '& *': {
                fontSize: '16px',
                color: '#FFF',
            },
            '& * + *': {
                fontSize: '14px',
            }
        },
        heart: {
            fontSize: '2rem',
            color: '#FFF',
        },
        fabButton: {
            display: 'flex',
            justifyContent: 'right',
            alignItems: 'center',
        },
        card: {
            paddingVertical: theme.spacing(1),
        }
    }),
);

const MovieCard = ({ movie }) => {
    const classes = useStyles();
    const [like, setLike] = useState(false);
    let navigate = useNavigate();

    const [myMoviesSession, setMyMoviesSession] = useStickyState([], "myMovies");

    useEffect(() => {
        if (myMoviesSession.find((sessionMovie) => sessionMovie === movie.imdbID)){
            setLike(true);
        }
    }, [myMoviesSession, movie.imdbID])

    const handleClick = (imdbID) => {
        if (myMoviesSession.find((movie) => movie === imdbID)){
            setMyMoviesSession(myMoviesSession.filter((movie) => movie !== imdbID));
            setLike(false);
        }else{
            setMyMoviesSession([...myMoviesSession, imdbID]);
            setLike(true);
        }   
    };

    const handleDetail = (id) => {
        navigate(`/detalle/?id=${id}`)
    };

    return (
        <Grid item md={3} sm={6} xs={12} justifyContent={'center'}>
            <Card className={classes.root} onClick={() => handleDetail(movie.imdbID)}>
                <Grid container className={classes.card}>
                    <Grid item>
                        <CardHeader className={classes.header}
                            title={movie.Title}
                            subheader={movie.Year}
                        />
                    </Grid>
                </Grid>
                <CardMedia
                    className={classes.media}
                    image={(movie.Poster === 'N/A') ? noPoster : movie.Poster}
                    title={movie.Title}
                />
            </Card>
            <Grid item className={classes.fabButton}>
                <Typography variant="h6" component="p">Add to favorites</Typography>
                <IconButton aria-label="add to favorites" onClick={() => handleClick(movie.imdbID)}>
                    {like ? <FavoriteIcon className={classes.heart} /> : <FavoriteBorderIcon className={classes.heart}/>}
                </IconButton>
            </Grid>
        </Grid>
    );
}

export default MovieCard;
