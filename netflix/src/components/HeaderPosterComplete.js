import React from 'react';
import { Grid, Box, Typography, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useNavigate } from 'react-router-dom';
import PosterTitles from './PosterTitles';
import PosterDescription from './PosterDescription';
import alien from '../assets/alien-film.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        zIndex: 1,
    },
    bg: {
        position: 'absolute',
        zIndex: -1,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
        backgroundImage: `url(${alien})`,
        backgroundSize: 'cover',
        height: '550px',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '100% 55%',
        opacity: '0.4',
        [theme.breakpoints.only('sm')]: {
            height: '660px',
        }
    },
    box: {
        padding: theme.spacing(6),
    },
    buttons: {
        textAlign: 'right',
        [theme.breakpoints.only('xs')]: {
            textAlign: 'left',
            padding: '0px',
            '& *': {
                padding: '0px',
            }
        },
    },
    highlight: {
        color: 'red',
        cursor: 'pointer',
        [theme.breakpoints.only('sm')]: {
            padding: theme.spacing(1),
        },
    }
}));

const HeaderPosterComplete = () => {
    const classes = useStyles();
    let navigate = useNavigate();

    const handleClick = () => {
        navigate('/')
    };

    return (
        <Box className={classes.root}>
            <Box className={classes.bg} />
            <Grid container className={classes.box}>
                <Grid item sm={6} xs={12} onClick={handleClick}>
                    <Typography variant="h4" component="p">Fake Netflix <span className={classes.highlight}>Movies</span></Typography>
                </Grid>
                <Grid item sm={6} xs={12} className={classes.buttons}>
                    <Button color="inherit" href="https://gitlab.com/danicolombo">Gitlab</Button>
                </Grid>
            </Grid>
            <PosterTitles />
            <PosterDescription />
        </Box>
    );
}

export default HeaderPosterComplete;
