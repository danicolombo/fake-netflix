import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Typography} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    box: {
        padding: theme.spacing(6),
    },
}));

const PosterDescription  = () => {
    const classes = useStyles();

    return (
        <>
            <Grid container className={classes.box}>
                <Grid item md={2} xs={6}>
                    <Typography variant="body1" component="p">Director</Typography>
                    <Typography variant="body2" component="p">Ridley Scott</Typography>
                </Grid>
                <Grid item md={2} xs={6}>
                    <Typography variant="body1" component="p">Writter</Typography>
                    <Typography variant="body2" component="p">Ridley Scott, Winona Rider</Typography>
                </Grid>
                <Grid item md={2} xs={6}>
                    <Typography variant="body1" component="p">Genres</Typography>
                    <Typography variant="body2" component="p">Horror | Sci-fi | Thriller</Typography>
                </Grid>
                <Grid item md={2} xs={6}>
                    <Typography variant="body1" component="p">Running time</Typography>
                    <Typography variant="body2" component="p">129 min</Typography>
                </Grid>
            </Grid>
        </>
    );
}

export default PosterDescription;
