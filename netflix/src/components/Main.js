import React from 'react';
import MovieList from './MovieList';
import HeaderPosterComplete from './HeaderPosterComplete';

const Main = () => {
  return (
      <>
        <HeaderPosterComplete/>
        <MovieList/>
      </>
  );
}

export default Main;
