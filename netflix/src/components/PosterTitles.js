import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Typography} from '@material-ui/core';
import {useNavigate} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    alienbox: {
        padding: theme.spacing(6),
        cursor: 'pointer',
    },
    titles: {
        [theme.breakpoints.only('xs')]: {
            fontSize: '4rem',
        },
    },
    imdb: {
        [theme.breakpoints.only('xs')]: {
            fontSize: '4rem',
        },
    },
}));

const PosterTitles = () => {
    const classes = useStyles();
    let navigate = useNavigate();

    const handleFirstClick = () => {
        const id = 'tt0078748';
        navigate(`/detalle/?id=${id}`)
    };

    return (
        <>
            <Grid container className={classes.alienbox} onClick={handleFirstClick}>
                <Grid item md={6} xs={12}>
                    <Typography variant="h3" component="p" className={classes.imdb}>IMDB</Typography>
                    <Typography variant="h2" component="p" className={classes.imdb}>9/10</Typography>
                </Grid>
                <Grid item md={6} xs={12}>
                    <Typography variant="h1" component="p" className={classes.titles}>ALIEN</Typography>
                    <Typography variant="h1" component="p" className={classes.titles}>COVENANT</Typography>
                </Grid>
            </Grid>
        </>
    );
}

export default PosterTitles;
