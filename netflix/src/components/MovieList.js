import React, {useState, useEffect} from 'react';
import {makeStyles, createStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MovieCard from './MovieCard';

const useStyles = makeStyles((theme) =>
    createStyles({
        container: {
            display: 'flex',
            padding: theme.spacing(3),
        },
        pager: {
            display: 'flex',
            justifyContent: 'center',
        }
    }),
);

const MovieList = () => {
    const classes = useStyles();
    const [movies, setMovies] = useState([]);

    const apyKey = process.env.REACT_APP_API_KEY;

    useEffect(() => {
        fetch(`http://www.omdbapi.com/?apikey=${apyKey}&s=Batman&page=2`)
            .then(res => res.json())
            .then(
                (res) => {
                    console.log('res', res.Search)
                    setMovies(res.Search)
                })
            .catch(
                (error) => {
                    console.log('error', error)
                }
            )
    }, [apyKey])

    return (
        <Grid container className={classes.container}>
            {movies?.map((movie) => (
                <MovieCard movie={movie} key={movie.imdbID}/>
            ))}
        </Grid>
    );
    
}

export default MovieList;
