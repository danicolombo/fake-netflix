import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Typography, Divider} from '@material-ui/core';
import noPoster from '../assets/no-film.jpg';
import HeaderDetail from './HeaderDetail';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        padding: `${theme.spacing(10)}px ${theme.spacing(40)}px`,
        [theme.breakpoints.only('sm')]: {
            padding: theme.spacing(6),
        },
        [theme.breakpoints.only('xs')]: {
            padding: theme.spacing(2),
        },
    }, 
    divider: {
        backgroundColor: '#FFF',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: '95%',
    },
    spacing: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    plot: {
        paddingRight: theme.spacing(3),
    },
    image: {
        width: '100%',
    },
    textContainer: {
        [theme.breakpoints.only('xs')]: {
            padding: theme.spacing(4),
        },
    }
}));

const MovieDetail = () => {
    const classes = useStyles();
    const [movie, setMovie] = useState();
    const params = new URLSearchParams(window.location.search)
    let id = params.get('id')

    const apyKey = process.env.REACT_APP_API_KEY;

    useEffect(() => {
        fetch(`http://www.omdbapi.com/?apikey=${apyKey}&i=${id}`)
            .then(res => res.json())
            .then((res) => {
                console.log('res', res)
                setMovie(res)
            })
            .catch((err) => console.log(err))
    }, [id, apyKey])

    return (
        <>
        <HeaderDetail/>
        <Grid container className={classes.container}>
            <Grid item sm={8} xs={12} className={classes.textContainer}>
                {movie &&
                <>   
                    <Grid container>
                        {movie.Title !== 'N/A' ? 
                        <Grid item xs={12} className={classes.plot}>                
                            <Typography variant="h2" component="p">{movie.Title}</Typography> 
                        </Grid>
                        : '' }
                    </Grid>
                    <Grid container>
                        <Grid item sm={6} xs={12}>
                            {movie.Genre !== 'N/A' ? <Typography variant="h6" component="p">{movie.Genre}</Typography> : '' }
                            {movie.Year !== 'N/A' ? <Typography variant="h6" component="p">{movie.Year}</Typography> : '' }
                        </Grid>
                        {movie.Director !== 'N/A' ? 
                            <Grid item sm={6} xs={12}>
                                <Typography variant="body1" component="p">Director</Typography>
                                <Typography variant="h6" component="p">{movie.Director}</Typography>
                            </Grid> 
                        : '' }
                    </Grid>
                    <Grid container className={classes.plot}>
                        <Grid item xs={12}>
                            {movie.Writers !== 'N/A' ? <Typography variant="h6" component="p">{movie.Writers}</Typography> : '' }
                            {movie.Production !== 'N/A' ? <Typography variant="body1" component="p">{movie.Production}</Typography> : ''} 
                            {movie.Plot !== 'N/A' ? <Typography variant="body2" component="p" className={classes.spacing}>{movie.Plot}</Typography> : ''} 
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider}/>
                    <Grid container>
                        {movie.Runtime !== 'N/A' ? 
                        <Grid item sm={3} xs={12}>
                            <Typography variant="body1" component="p">Runtime</Typography> 
                            <Typography variant="h6" component="p">{movie.Runtime}</Typography>
                        </Grid>
                        : ''}

                        {movie.Rated !== 'N/A' ?
                        <Grid item sm={3} xs={12}>
                            <Typography variant="body1" component="p">Rated</Typography>
                            <Typography variant="h6" component="p">{movie.Rated}</Typography>
                        </Grid> 
                        : ''}

                        {movie.Language !== 'N/A' ? 
                        <Grid item sm={3} xs={12}>
                            <Typography variant="body1" component="p">Language</Typography>
                            <Typography variant="h6" component="p">{movie.Language}</Typography>
                        </Grid> 
                        : ''}
                    
                        {movie.Country !== 'N/A' ? 
                        <Grid item sm={3} xs={12}>
                            <Typography variant="body1" component="p">Country</Typography>
                            <Typography variant="h6" component="p">{movie.Country}</Typography>
                        </Grid> 
                        : ''}
                    </Grid>
                </>
                }
            </Grid>
            <Grid item sm={4} xs={12} spacing={1}>
                {movie && <img className={classes.image} src={(movie.Poster === 'N/A') ? noPoster : movie.Poster} alt={movie.Title} />}
            </Grid>
        </Grid>
    </>
    );
}

export default MovieDetail;
