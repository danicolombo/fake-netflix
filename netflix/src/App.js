import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {ThemeProvider} from '@material-ui/core/styles';
import theme from './theme';
import Main from './components/Main';
import MovieDetail from './components/MovieDetail';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<Main />}/>
          <Route exact path='/detalle' element={<MovieDetail />}/>
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
